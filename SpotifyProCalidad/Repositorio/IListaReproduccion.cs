using System.Collections.Generic;
using SpotifyProCalidad.Models;

namespace SpotifyProCalidad.Repositorio
{
    public interface IListaReproduccion
    {
        public List<ListaRepoduccion> GetAllListaRepoduccion();
        public List<int> IdMiListaReproduccion(int IdLista);

        public void EliminarLista(ListaRepoduccion ListaAEliminar);
    
        public void AgregarLista(string NombreLista,int IdUser);

        public void CambioEStadoListaReproduccion(bool Estado, int IdLista);
        
        
    }
}