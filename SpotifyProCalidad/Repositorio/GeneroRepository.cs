using System.Collections.Generic;
using System.Linq;
using SpotifyProCalidad.BD;
using SpotifyProCalidad.Models;

namespace SpotifyProCalidad.Repositorio
{
    public class GeneroRepository : IGenero
    {
        SpotifyContext _context = new SpotifyContext();
        public List<Genero> GetAllGeneros()
        {
            var generos = _context.Generos.ToList();
            return generos;
        }
    }
}