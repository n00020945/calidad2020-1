using System.Collections.Generic;
using System.Linq;
using SpotifyProCalidad.BD;
using SpotifyProCalidad.Models;

namespace SpotifyProCalidad.Repositorio
{
    public class AmistadRepository : IAmistad
    {
        SpotifyContext _context = new SpotifyContext();
  
        public List<Amistad> MisAmistades()
        {
            var MisAmistades = _context.Amigos.ToList();

            return MisAmistades;
        }
    }
}