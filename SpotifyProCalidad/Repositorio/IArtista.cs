using System.Collections.Generic;
using SpotifyProCalidad.Models;

namespace SpotifyProCalidad.Repositorio
{
    public interface IArtista
    {
        public List<Artista> GetAllArtistas();
        public Dictionary<int, string> DictionaryArtistas();
    }
}