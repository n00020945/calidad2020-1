using System.Collections.Generic;
using SpotifyProCalidad.Models;

namespace SpotifyProCalidad.Repositorio
{
    public interface IDetalleCancionArtista
    {
        public List<DetalleCancionArtista> GetAllDetalleCancionArtista();
    }
}