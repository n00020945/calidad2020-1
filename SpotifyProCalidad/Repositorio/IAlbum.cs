using System.Collections.Generic;
using SpotifyProCalidad.Models;

namespace SpotifyProCalidad.Repositorio
{
    public interface IAlbum
    {
        public List<Album> GetAllAlbumnes();
    }
}