using System.Collections.Generic;
using SpotifyProCalidad.Models;

namespace SpotifyProCalidad.Repositorio
{
    public interface IAmistad
    {
        public List<Amistad> MisAmistades();
    }
}