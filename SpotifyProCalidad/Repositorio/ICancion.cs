using System.Collections.Generic;
using System.Linq;
using SpotifyProCalidad.Models;

namespace SpotifyProCalidad.Repositorio
{
    public interface ICancion
    {
        public List<Cancion> GetAllCanciones();
        public IQueryable<Cancion> GetAllCancionesIQueryable();

        public List<Cancion> BusquedaCancion(string busqueda);

        public void AgregarCancion(int IdCancion, ListaRepoduccion ListaFavoritos);

        public List<Cancion> MisCanciones(List<int> ListaCanciones);
        
        public void AgregarCancionAListaReproduccion(int IdCancion, int IdLista);
        
        public void EliminarCancionAListaReproduccion(int IdCancion, int IdLista);
    }
}