using System.Collections.Generic;
using SpotifyProCalidad.Models;

namespace SpotifyProCalidad.Repositorio
{
    public interface IDetalleListaReproduccionCancion
    {
        public List<DetalleListaReproduccionCancion> GetAllDetalleListaReproduccionCancion();
    }
}