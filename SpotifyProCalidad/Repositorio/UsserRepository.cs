using System.Collections.Generic;
using System.Linq;
using SpotifyProCalidad.BD;
using SpotifyProCalidad.Models;

namespace SpotifyProCalidad.Repositorio
{
    public class UsserRepository : IUsser
    {
        SpotifyContext _context = new SpotifyContext();
        public List<Usser> GetAllUsuarios()
        {
            var usuarios = _context.Ussers.ToList();
            return usuarios;
        }
    }
}