using System.Collections.Generic;
using System.Linq;
using SpotifyProCalidad.BD;
using SpotifyProCalidad.Models;

namespace SpotifyProCalidad.Repositorio
{
    public class DetalleListaReproduccionCancionRepository : IDetalleListaReproduccionCancion
    {
        SpotifyContext _context = new SpotifyContext();
        public List<DetalleListaReproduccionCancion> GetAllDetalleListaReproduccionCancion()
        {
            var detalleListaReproduccionCanciones = _context.DetalleListaReproduccionCanciones.ToList();
            return detalleListaReproduccionCanciones;
        }
    }
}