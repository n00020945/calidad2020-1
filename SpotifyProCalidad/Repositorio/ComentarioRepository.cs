using System.Collections.Generic;
using System.Linq;
using SpotifyProCalidad.BD;
using SpotifyProCalidad.Models;

namespace SpotifyProCalidad.Repositorio
{
    public class ComentarioRepository : IComentario
    {
        SpotifyContext _context = new SpotifyContext();
        public void GuardarCOmentario(int IdLista, string descripcion, int Idusuario)
        {
            Comentario comentarioNuevo = new Comentario();
            comentarioNuevo.Descripcion = descripcion;
            comentarioNuevo.IdUsuario = Idusuario;
            _context.Comentarios.Add(comentarioNuevo);
        }

        public List<Comentario> GetALlComentarios()
        {
            var comentarios = _context.Comentarios.ToList();
            return comentarios;
        }

        public List<Comentario> comentariosLista(int IdLista)
        {
            var detalle = _context.DetalleListaReproduccionComentarios.Where(o => o.IdListaReproduccion == IdLista).ToList().
                Select(o=>o.IdComentario)
                .ToList();
            List<Comentario> comentariosLista = new List<Comentario>();
            var comentarios = _context.Comentarios.ToList();

            foreach (var item in comentarios)
            {
                if (detalle.Contains(item.Id))
                {
                    comentariosLista.Add(item);
                }
            }

            return comentariosLista;

        }
    }
}