using System.Collections.Generic;
using SpotifyProCalidad.Models;

namespace SpotifyProCalidad.Repositorio
{
    public interface IUsser
    {
        public List<Usser> GetAllUsuarios();
    }
}