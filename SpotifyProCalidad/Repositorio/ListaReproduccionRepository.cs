using System;
using System.Collections.Generic;
using System.Linq;
using SpotifyProCalidad.BD;
using SpotifyProCalidad.Models;

namespace SpotifyProCalidad.Repositorio
{
    public class ListaReproduccionRepository : IListaReproduccion
    {
        SpotifyContext _context = new SpotifyContext();
        public List<ListaRepoduccion> GetAllListaRepoduccion()
        {
            var listaReproducciones = _context.ListaReproducciones.ToList();
            return listaReproducciones;
        }

        public List<int> IdMiListaReproduccion(int IdLista)
        {
            var detalleId=_context.DetalleListaReproduccionCanciones.ToList()
                .Where(o => o.IdListaReproduccion == IdLista).ToList().Select(o => o.IdCancion).ToList();
            return detalleId;
        }
//Nuevos metodos eliminar lista y agregar lista
        public void EliminarLista(ListaRepoduccion ListaAEliminar)
        {
            _context.ListaReproducciones.Remove(ListaAEliminar);
            _context.SaveChanges();
        }

        public void AgregarLista(string NombreLista, int IdUser)
        {
            ListaRepoduccion NuevaLista = new ListaRepoduccion();
            NuevaLista.Nombre = NombreLista;
            NuevaLista.FechaCreacion =DateTime.Now;
            NuevaLista.UsserId = IdUser;
            NuevaLista.Estado = false;
            _context.ListaReproducciones.Add(NuevaLista);
            _context.SaveChanges();
        }

        public void CambioEStadoListaReproduccion(bool Estado, int IdLista)
        {
            var ListaCambiarEstado = _context.ListaReproducciones.Where(o => o.Id == IdLista).FirstOrDefault();
            ListaCambiarEstado.Estado = Estado;
            _context.SaveChanges();

        }

      

     
        
    }
}