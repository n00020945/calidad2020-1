using System.Collections.Generic;
using System.Linq;
using SpotifyProCalidad.BD;
using SpotifyProCalidad.Models;

namespace SpotifyProCalidad.Repositorio
{
    public class DetalleCancionArtistaRepository : IDetalleCancionArtista
    {
        SpotifyContext _context = new SpotifyContext();
        public List<DetalleCancionArtista> GetAllDetalleCancionArtista()
        {
            var detalleCancionArtista = _context.DetalleCancionArtistas.ToList();
            return detalleCancionArtista;
        }
    }
}