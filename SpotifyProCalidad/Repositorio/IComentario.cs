using System.Collections.Generic;
using SpotifyProCalidad.Models;

namespace SpotifyProCalidad.Repositorio
{
    public interface IComentario
    {
        public void GuardarCOmentario(int IdLista, string descripcion, int Idusuario);
        public List<Comentario> GetALlComentarios();

        public List<Comentario> comentariosLista(int IdLista);
    }
}