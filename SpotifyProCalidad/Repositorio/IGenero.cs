using System.Collections.Generic;
using SpotifyProCalidad.Models;

namespace SpotifyProCalidad.Repositorio
{
    public interface IGenero
    {
        public List<Genero> GetAllGeneros();
    }
}