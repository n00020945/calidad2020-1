﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Media;
using System.Runtime.InteropServices;
using System.Text;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using SpotifyProCalidad.BD;
using SpotifyProCalidad.Extensions;
using SpotifyProCalidad.Models;
using SpotifyProCalidad.Repositorio;

namespace SpotifyProCalidad.Controllers
{
    [Authorize]
    public class ListaReproduccionController : Controller
    {
       

        IAlbum _album = new AlbumRepository();
        IArtista _artista = new ArtistaRepository();
        ICancion _cancion = new CancionRepository();
        IDetalleCancionArtista _detalleCancionArtista = new DetalleCancionArtistaRepository();

        IDetalleListaReproduccionCancion _detalleListaReproduccionCancion =
            new DetalleListaReproduccionCancionRepository();

        IGenero _genero = new GeneroRepository();
        IListaReproduccion _listaReproduccion = new ListaReproduccionRepository();
        IComentario _comentario=new ComentarioRepository();
        IUsser _usser = new UsserRepository();

      
 

        public IActionResult Index()
        {
            var sesion = HttpContext.Session.Get<Usser>("sessionUser");
            
            var ListasDeReproducciones = _listaReproduccion.GetAllListaRepoduccion().Where(o=>o.UsserId==sesion.Id).ToList();
            
            return View(ListasDeReproducciones);
           // return RedirectToAction("DetalleListaReproduccion");
        }

        public IActionResult DetalleListaReproduccion(int IdLista)
        {
            var sesion = HttpContext.Session.Get<Usser>("sessionUser");
            var ListaCanciones = _listaReproduccion.IdMiListaReproduccion(IdLista);
            var CancionesDetalle = _cancion.MisCanciones(ListaCanciones);

            ViewBag.DetalleDeCanciones = CancionesDetalle;
            var artistas = _artista.GetAllArtistas();
            var generos = _genero.GetAllGeneros();
            var DetalleCancionArtista = _detalleCancionArtista.GetAllDetalleCancionArtista();
            var Albumnes = _album.GetAllAlbumnes();
            var NombreArtista = _artista.DictionaryArtistas();
            ViewBag.Artista = NombreArtista;
            ViewBag.Usser = sesion;
            ViewBag.Generos = generos;
            ViewBag.DetalleCancionArtista = DetalleCancionArtista;
            ViewBag.Albumnes = Albumnes;
            ViewBag.Favoritos=_listaReproduccion.GetAllListaRepoduccion().FirstOrDefault(o => o.Nombre == "Favoritos" && o.UsserId == sesion.Id);
            return View();
        }

        public IActionResult AgregarCancionFavoritos(int IdCancion)
        {
            var sesion = HttpContext.Session.Get<Usser>("sessionUser");
            var ListaFavoritos = _listaReproduccion.GetAllListaRepoduccion().FirstOrDefault(o => o.Nombre == "Favoritos" && o.UsserId ==sesion.Id);
            _cancion.AgregarCancion(IdCancion,ListaFavoritos);
            return RedirectToAction("Perfil", "PaginaPrincipal");

        }
        
        public IActionResult EliminarLista (int IdLista)
        {
            var sesion = HttpContext.Session.Get<Usser>("sessionUser");
            var ListaAEliminar = _listaReproduccion.GetAllListaRepoduccion().FirstOrDefault(o => o.Id == IdLista && o.UsserId ==sesion.Id);
            _listaReproduccion.EliminarLista(ListaAEliminar);
            return RedirectToAction("Perfil", "PaginaPrincipal");

        }
        public IActionResult AgregarLista (string NombreLista)
        {
            var sesion = HttpContext.Session.Get<Usser>("sessionUser");
            var IdUser = sesion.Id;
            _listaReproduccion.AgregarLista(NombreLista,IdUser);
            return RedirectToAction("Perfil", "PaginaPrincipal");
        }
        public IActionResult CambiarEstadoLista (bool Estado, int IdLista)
        {
            _listaReproduccion.CambioEStadoListaReproduccion(Estado, IdLista);
            
            return RedirectToAction("Perfil", "PaginaPrincipal");
        }
        public IActionResult AgregarCancionAListaReproduccion (int IdCancion, int IdLista)
        {
            _cancion.AgregarCancionAListaReproduccion(IdCancion,IdLista);
            
            return RedirectToAction("Perfil", "PaginaPrincipal");
        }
        
        public IActionResult EliminarCancionAListaReproduccion (int IdCancion, int IdLista)
        {
            _cancion.EliminarCancionAListaReproduccion(IdCancion,IdLista);
            
            return RedirectToAction("Perfil", "PaginaPrincipal");
        }
        //DetalleListaAmigo
        public IActionResult DetalleLista(int IdLista)
        {
            var ListaCanciones = _listaReproduccion.IdMiListaReproduccion(IdLista);
            var CancionesDetalle = _cancion.MisCanciones(ListaCanciones);


            var comentarios =_comentario.comentariosLista(IdLista);
            
            var usuarios = _usser.GetAllUsuarios();
            Dictionary<int,Usser> usuariosDictionary = new Dictionary<int, Usser>();
            foreach (var item in usuarios)
            {
                usuariosDictionary.Add(item.Id,item);
            }
            
            
            ViewBag.ComentariosDeLaLista = comentarios;
            ViewBag.DiccionarioDeUsuarios = usuariosDictionary;
            ViewBag.Canciones = CancionesDetalle;
            
            return View();
        }
  


    }
}
