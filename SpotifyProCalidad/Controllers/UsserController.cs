using System;
using System.Linq;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using SpotifyProCalidad.BD;
using SpotifyProCalidad.Extensions;
using SpotifyProCalidad.Models;

namespace SpotifyProCalidad.Controllers
{
   
    public class UsserController : Controller
    {
    SpotifyContext context = new SpotifyContext();
        //c GET
        [Authorize]
        public IActionResult Index()
        {
            //sesion
            var sesion = HttpContext.Session.Get<Usser>("sessionUser");
            
            var canciones = context.Canciones.ToList();
            ViewBag.canciones = canciones;


            return RedirectToAction("Perfil","PaginaPrincipal");
        }
   
        [HttpPost]
        public IActionResult Registrarse(Usser usuario)
        {
            usuario.FechaCreacionUsuario = DateTime.Now;
            
            usuario.Imagen ="UserNew.png";
            

            if (ModelState.IsValid)
            {
                var agregarUsuario = context.Add(usuario);
                context.SaveChanges();
            
                var user = context.Ussers.Where(o => o.Equals(usuario)).FirstOrDefault();
                ListaRepoduccion favoritos = new ListaRepoduccion();
                favoritos.Nombre = "Favoritos";
                favoritos.FechaCreacion =DateTime.Now;
                favoritos.UsserId = user.Id;
                favoritos.Estado = false;
                context.ListaReproducciones.Add(favoritos);
                context.SaveChanges();
                return RedirectToAction("Index","Home");
            }


            return RedirectToAction("Index", "Home");

        }
        
    }
}