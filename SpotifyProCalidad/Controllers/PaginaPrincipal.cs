using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using SpotifyProCalidad.BD;
using SpotifyProCalidad.Extensions;
using SpotifyProCalidad.Models;
using SpotifyProCalidad.Repositorio;


namespace SpotifyProCalidad.Controllers
{
    [Authorize]
    public class PaginaPrincipal : Controller
    {
        IAlbum _album = new AlbumRepository();
        IArtista _artista = new ArtistaRepository();
        ICancion _cancion = new CancionRepository();
        IDetalleCancionArtista _detalleCancionArtista = new DetalleCancionArtistaRepository();
        IUsser _usuarios = new UsserRepository();
        IAmistad _amistad = new AmistadRepository();
        IComentario _comentario = new ComentarioRepository();

        IDetalleListaReproduccionCancion _detalleListaReproduccionCancion =
            new DetalleListaReproduccionCancionRepository();

        IGenero _genero = new GeneroRepository();
        IListaReproduccion _listaReproduccion = new ListaReproduccionRepository();

        public IActionResult Perfil(string busqueda = "")
        {
            var sesion = HttpContext.Session.Get<Usser>("sessionUser");
            var generos = _genero.GetAllGeneros();
            var DetalleCancionArtista = _detalleCancionArtista.GetAllDetalleCancionArtista();
            var Albumnes = _album.GetAllAlbumnes();
            var cancionesBuesquedaQuery = _cancion.BusquedaCancion(busqueda);
            var NombreArtista = _artista.DictionaryArtistas();
            var detalleLista = _listaReproduccion.GetAllListaRepoduccion()
                .FirstOrDefault(o => o.Nombre == "Favoritos" && o.UsserId == sesion.Id);
            var DetalleListaCanciones = _detalleListaReproduccionCancion.GetAllDetalleListaReproduccionCancion()
                .Where(o => o.IdListaReproduccion == detalleLista.Id).ToList();
            var Favoritos = _listaReproduccion.GetAllListaRepoduccion()
                .FirstOrDefault(o => o.Nombre == "Favoritos" && o.UsserId == sesion.Id);
            var DetalleListaReproduccion = DetalleListaCanciones.Select(o => o.IdCancion).ToList();


            ViewBag.Artista = NombreArtista;
            ViewBag.Usser = sesion;
            ViewBag.Generos = generos;
            ViewBag.DetalleCancionArtista = DetalleCancionArtista;
            ViewBag.Albumnes = Albumnes;
            ViewBag.Favoritos = Favoritos;
            ViewBag.DetalleNumeros = DetalleListaReproduccion;
            

            return View(cancionesBuesquedaQuery);
        }

        //nuevos cambios controlador y vista
        public IActionResult MisAmigos(string busqueda = "")
        {
            List<Usser> MisAmigos = new List<Usser>();
            var sesion = HttpContext.Session.Get<Usser>("sessionUser");
            var UsuariosAmigos = _amistad.MisAmistades().Where(o => o.idLocal == sesion.Id).ToList().Select(o=>o.idAmigo).ToList();
            var usuarios = _usuarios.GetAllUsuarios().ToList();
            
            
            foreach (var item in usuarios)
            {
                if (UsuariosAmigos.Contains(item.Id))
                {
                    MisAmigos.Add(item);
                }

            }

            ViewBag.Amigos = MisAmigos;
            return View();
        }
        
        //playlistAmigos
        public IActionResult PlaylistAmigos(int IdAmigo)
        {
            var playlistAmigo = _listaReproduccion.GetAllListaRepoduccion().Where(o => o.UsserId == IdAmigo && !o.Estado).ToList();
            ViewBag.ListasReproducciones =playlistAmigo ;
            return View();
        }
        
        //Agregar comentario
        public IActionResult AgregarComentario(int IdLista, string descripcion)
        {
            var sesion = HttpContext.Session.Get<Usser>("sessionUser");
            _comentario.GuardarCOmentario(IdLista,descripcion,sesion.Id);
            
            

            return RedirectToAction("Perfil", "PaginaPrincipal");
        }
    }
}