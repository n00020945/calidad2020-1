using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Text;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using SpotifyProCalidad.BD;
using SpotifyProCalidad.Extensions;

public class AuthController : Controller
{
    SpotifyContext context = new SpotifyContext();
    private IConfiguration configuration;

    public AuthController(IConfiguration configuration)
    {
        this.configuration = configuration;
    }

   
   

    [HttpPost]
    public IActionResult Login(String usern, String pass)
    {
        
        var userv = context.Ussers.FirstOrDefault(o => o.Nickname == usern && o.Password == pass);

        if (userv == null)
            return RedirectToAction("Index","Home");


        HttpContext.Session.Set("sessionUser", userv);


        var claims = new List<Claim>()
        {
            new Claim(ClaimTypes.Name, userv.Nickname),
        };

        var userIdentity = new ClaimsIdentity(claims, "login");
        var principal = new ClaimsPrincipal(userIdentity);

        HttpContext.SignInAsync(principal);

        return RedirectToAction("Perfil", "PaginaPrincipal");
    }

    [HttpGet]
    public IActionResult Logout()
    {
        HttpContext.SignOutAsync();
        return RedirectToAction("Index", "Home");
    }

    public String GetBashPassword(String pass)
    {
        string token = configuration.GetValue<string>("Token");
        pass = pass + token;
        var sha = SHA256.Create();
        var hashdata = sha.ComputeHash(Encoding.Default.GetBytes(pass));
        return Convert.ToBase64String(hashdata);
    }
}
